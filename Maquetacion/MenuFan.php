<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link href="styles/MenuFan.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
         <?php
        require_once 'bbdd.php';
        session_start();
        if (isset($_SESSION["tipo"]) && $_SESSION["tipo"] == 3) {
            
        ?>
        <div>
            <input type="button" onclick="location.href = 'PerfilFan.php'" value="Ver datos de perfil" name="modificar">
        </div>
        <div class="tab">
             <p class="text">Listado de conciertos</p><br>

            <table class="tablaFan">
                <tr>
                    <th>Concierto</th>
                    <th>Músico</th>
                    <th>Local</th>
                    <th>Día</th>  
                    <th>Hora</th>
                    <th>Ciudad</th>
                    <th>Género</th>
                    <th>Votar</th>
                </tr>
                <?php
                $aprobados = selectConciertosAprobados();
                    while ($fila = mysqli_fetch_assoc($aprobados)){
                        echo"<tr><td>" . $fila["nombreconcierto"] . "</td>";
                        echo"<td>" . $fila["nombreartistico"] . "</td>";
                        echo"<td>" . $fila["nombrelocal"] . "</td>";
                        echo"<td>" . $fila["dia"] . "</td>";
                        echo"<td>" . $fila["hora"] . "</td>";
                        echo"<td>" . $fila["nombreciudad"] . "</td>";
                        echo"<td>" . $fila["nombregenero"] . "</td>";
                        echo"<td>";
                        if(haVotadoConcierto($_SESSION["idusuario"],$fila["idconcierto"])){
                            echo "<form action='' method='post'>
                            <input type='submit' value='Quitar Voto' name='quitarVotoConcierto'>
                            <input type='hidden' name='concierto' value='" . $fila["idconcierto"] . "'></form></td></tr>";
                        }else{
                            echo "<form action='' method='post'>
                            <input type='submit' value='Votar' name='votarConcierto'>
                            <input type='hidden' name='concierto' value='" . $fila["idconcierto"] . "'></form></td></tr>";
                        }
                    }
                    ?>
            </table>
        </div>
        <div class="tab">
            <p class="text">Listado de músicos</p><br>

            <table class="tablaFan">
                <tr>
                    <th>Nombre Artístico</th>
                    <th>Género</th>
                    <th>Web</th>
                    <th>Votar</th>
                </tr>
                <?php
                $musicos = selectMusicos();
                while ($fila = mysqli_fetch_assoc($musicos)){
                    echo"<tr><td>" . $fila["nombreartistico"] . "</td>";
                    echo"<td>" . $fila["nombregenero"] . "</td>";
                    echo"<td>" . $fila["web"] . "</td>";
                    echo"<td>";
                    if(haVotadoMusico($_SESSION["idusuario"],$fila["idmusico"])){
                        echo"<form action='' method='post'>
                            <input type='submit' value='Quitar Voto' name='quitarVotoMusico'>
                            <input type='hidden' name='musico' value='" . $fila["idmusico"] . "'></form></td></tr>";
                    }else{
                        echo"<form action='' method='post'>
                            <input type='submit' value='Votar' name='votarMusico'>
                            <input type='hidden' name='musico' value='" . $fila["idmusico"] . "'></form></td></tr>";
                    }
                }
                ?>
            </table>
        </div>
        <div><br><br>
           <form action="Homepage.php" method="POST">
            <a href="logout.php">Logout</a>
        </form>  
        </div>
        <?php
        } else {
            echo "No tienes permiso para ver esta pagina<br>";
        }
        if (isset($_POST["votarConcierto"])){
            $idconcierto = $_POST["concierto"];
            $idfan = $_SESSION["idusuario"];
            votarConcierto($idconcierto, $idfan);
        }
        if(isset($_POST["quitarVotoConcierto"])){
            $idconcierto = $_POST["concierto"];
            $idfan = $_SESSION["idusuario"];
            quitarVotoConcierto($idconcierto, $idfan);
        }
        if (isset($_POST["votarMusico"])){
            $idmusico = $_POST["musico"];
            $idfan = $_SESSION["idusuario"];
            votarMusico($idmusico, $idfan);
        }
        if(isset($_POST["quitarVotoMusico"])){
            $idmusico = $_POST["musico"];
            $idfan = $_SESSION["idusuario"];
            quitarVotoMusico($idmusico, $idfan);
        }
        ?>
    </body>
</html>
