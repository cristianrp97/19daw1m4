<html>
    <head>
        <meta charset="UTF-8">
        <link href="styles/PerfilFan.css" rel="stylesheet" type="text/css"/>
        <title></title>
    </head>
    <body>
        <?php
        session_start();
        if (isset($_SESSION["tipo"]) && $_SESSION["tipo"] == 3) {
            ?>
            <div>
                <h2>INFORMACIÓN DEL PERFIL (FAN)</h2>
                <div class="div1">
                    <?php
                    require_once 'bbdd.php';
                    $fans = selectAllFans($_SESSION["idusuario"]);
                    $fila = mysqli_fetch_assoc($fans);
                    $nombre = $fila["nombre"];
                    $apellidos = $fila["apellidos"];
                    $sexo = $fila["sexo"];
                    $telefono = $fila["telefono"];
                    $email = $fila["email"];
                    $nombreusuario = $fila["nombreusuario"];
                    ?>
                    <table style="width:100%; height: 100%;">
                        <tr>
                            <th>INFORMACIÓN GENERAL</th>
                        </tr>
                        <tr>
                            <td>Nombre:
                                <?php
                                echo $nombre;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Apellidos:
                                <?php
                                echo $apellidos;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Sexo:
                                <?php
                                echo $sexo;
                                ?>
                            </td>                    
                        </tr>
                    </table>
                </div><div class="div3">
                    <table style="width:100%; height: 100%;">
                        <tr>
                            <th>Nombre de Usuario:
                                <?php
                                echo $nombreusuario;
                                ?>
                            </th>
                        </tr>
                    </table>
                </div>
            </div>
            <div>
                <div class="div2">
                    <table style="width:100%; height: 100%;">
                        <tr>
                            <th>INFORMACIÓN ADICIONAL</th>
                        </tr>
                        <tr>
                            <td>Telefono:
                                <?php
                                echo $telefono;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Email:
                                <?php
                                echo $email;
                                ?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="divimg">
                    <img style="width: 300px; height: 300px;"src="user.jpg" alt=""/>
                </div>
            </div>
            <input type="button" onclick="location.href = 'modPerfil.php'" value="Modificar perfil" name="modPerfil">
            <input type="button" onclick="location.href = 'MenuFan.php'" value="Volver" name="volver">
            <form action="Homepage.php" method="POST">
                <a href="logout.php">Logout</a>
            </form> 
            <?php
        } else {
            echo "No tienes permiso para ver esta pagina<br>";
        }
        ?>
    </body>
</html>
