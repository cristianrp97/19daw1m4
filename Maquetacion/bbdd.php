<?php

function conectar() {
$conexion = mysqli_connect("localhost", "root", "", "proyectotransversal");
if (!$conexion) {
die("Error al conectar: " . mysqli_connect_error());
}
return $conexion;
}

function desconectar($conexion) {
mysqli_close($conexion);
}

function login($nombreusuario) {
$c = conectar();
$select = "select * from usuario 
LEFT JOIN musico ON usuario.idusuario = musico.idmusico 
where nombreusuario='$nombreusuario'";
$resultado = mysqli_query($c, $select);
$fila = mysqli_fetch_assoc($resultado);
desconectar($c);
return $fila;
}

function registrarLocal($nombreusuario, $pass, $nombre, $email, $telefono, $ciudad, $tipo, $direccion, $aforo) {
$c = conectar();
$passcif = password_hash($pass, PASSWORD_DEFAULT);
$insert1 = "insert into proyectotransversal.usuario values (null, '$nombreusuario', '$passcif', '$nombre', '$email', $telefono, $ciudad, $tipo)";
if (mysqli_query($c, $insert1)) {
$idlocal = mysqli_insert_id($c);
$insert2 = "insert into proyectotransversal.Locales values ($idlocal, '$direccion', $aforo, null)";
if (mysqli_query($c, $insert2)) {
$resultado = "ok";
} else {
$resultado = mysqli_error($c);
}
} else {
$resultado = mysqli_error($c);
}
desconectar($c);
return $resultado;
}

function registrarFan($nombreusuario, $pass, $nombre, $apellidos, $nacimiento, $sexo, $email, $telefono, $ciudad, $tipo) {
$c = conectar();
$passcif = password_hash($pass, PASSWORD_DEFAULT);
$insert1 = "insert into proyectotransversal.usuario values (null, '$nombreusuario', '$passcif', '$nombre', '$email', $telefono, $ciudad, $tipo)";
if (mysqli_query($c, $insert1)) {
$idfan = mysqli_insert_id($c);
$insert2 = "insert into proyectotransversal.fan values ($idfan, '$apellidos', '$sexo', '$nacimiento')";
if (mysqli_query($c, $insert2)) {
$resultado = "ok";
} else {
$resultado = mysqli_error($c);
}
} else {
$resultado = mysqli_error($c);
}
desconectar($c);
return $resultado;
}

function registrarMusico($nombreusuario, $pass, $nombre, $genero, $web, $artistico, $numcomponentes, $email, $telefono, $ciudad, $tipo) {
$c = conectar();
$passcif = password_hash($pass, PASSWORD_DEFAULT);
$insert1 = "insert into proyectotransversal.usuario values (null, '$nombreusuario', '$passcif', '$nombre', '$email', $telefono, $ciudad, $tipo)";
if (mysqli_query($c, $insert1)) {
$idmusico = mysqli_insert_id($c);
$insert2 = "insert into proyectotransversal.musico values ($idmusico, '$artistico', '$genero', '$web', '$numcomponentes')";
if (mysqli_query($c, $insert2)) {
$resultado = "ok";
} else {
$resultado = mysqli_error($c);
}
} else {
$resultado = mysqli_error($c);
}

desconectar($c);
return $resultado;
}

function insertarconcierto($nombre, $dia, $hora, $pago, $idusuario, $genero) {
$c = conectar();
$insert = "insert into proyectotransversal.concierto values (null,'$nombre',1,'$dia', '$hora', $pago, $idusuario, '$genero', null)";
if (mysqli_query($c, $insert)) {
$resultat = "ok";
} else {
$resultat = mysqli_error($c);
}
return $resultat;
}

function eliminarConcierto($id) {
$c = conectar();
$delete = "delete from concierto where idconcierto='$id'";
if (mysqli_query($c, $delete)) {
$resultado = true;
} else {
$resultado = mysqli_error($c);
}
desconectar($c);
return $resultado;
}

function verificar($nombreusuario, $password) {
$c = conectar();
$select = "select * from usuario where nombreusuario='$nombreusuario'";
$resultado = mysqli_query($c, $select);
if (mysqli_num_rows($resultado) == 0) { // usuario incorrecto
$resultado = false;
} else {
$fila = mysqli_fetch_assoc($resultado);
$resultado = password_verify($password, $fila["pass"]);
}
desconectar($c);
return $resultado;
}

function selectCiudad() {
$c = conectar();
$select = "select * from ciudad";
$resultado = mysqli_query($c, $select);
desconectar($c);
return $resultado;
}

function selectGenero() {
$c = conectar();
$select = "select * from genero";
$resultado = mysqli_query($c, $select);
desconectar($c);
return $resultado;
}

function selectAllMusicos($id) {
$c = conectar();
$select = "select * from usuario inner join musico on usuario.idusuario = musico.idmusico where idusuario = $id";
$resultado = mysqli_query($c, $select);
desconectar($c);
return $resultado;
}

function selectAllFans($id) {
$c = conectar();
$select = "select * from usuario inner join fan on usuario.idusuario = fan.idfan where idusuario = $id";
$resultado = mysqli_query($c, $select);
desconectar($c);
return $resultado;
}

function selectAllLocales($id) {
$c = conectar();
$select = "select * from usuario inner join locales on usuario.idusuario = locales.idlocal where idusuario = $id";
$resultado = mysqli_query($c, $select);
desconectar($c);
return $resultado;
}

function selectAllConciertos() {
$c = conectar();
$select = "select * from concierto inner join usuario on concierto.locales=usuario.idusuario inner join musico on concierto.musico=musico.idmusico";
$resultado = mysqli_query($c, $select);
desconectar($c);
return $resultado;
}

function selectConciertos() {
$c = conectar();
$select = "select * from concierto ";
$resultado = mysqli_query($c, $select);
desconectar($c);
return $resultado;
}

function modLocal($nombreusuario, $nombre, $email, $telefono, $direccion, $aforo) {
$c = conectar();
$modificar = "update usuario inner join locales on usuario.idusuario = locales.idlocal "
. "set nombre='$nombre', email='$email', telefono='$telefono', direccion='$direccion', aforo='$aforo' "
. "where nombreusuario='$nombreusuario'";
if (mysqli_query($c, $modificar)) {
$resultado = "ok";
} else {
$resultado = "ERROR: " . mysqli_error($c);
}
desconectar($c);
return $resultado;
}

function modMusico($nombreusuario, $nombre, $email, $telefono, $web, $numcomponentes) {
$c = conectar();
$modificar = "update usuario inner join musico on usuario.idusuario = musico.idmusico "
. "set nombre='$nombre', email='$email', telefono='$telefono', web='$web', "
. "numcomponentes='$numcomponentes' "
. "where nombreusuario='$nombreusuario'";
echo $modificar;
if (mysqli_query($c, $modificar)) {
$resultado = "ok";
} else {
$resultado = "ERROR: " . mysqli_error($c);
}
desconectar($c);
return $resultado;
}

function modFan($nombreusuario, $nombre, $apellidos, $email, $telefono, $sexo) {
$c = conectar();
$modificar = "update usuario inner join fan on usuario.idusuario = fan.idfan "
. "set nombre='$nombre', apellidos='$apellidos', email='$email', telefono='$telefono', sexo='$sexo' "
. "where nombreusuario='$nombreusuario'";
if (mysqli_query($c, $modificar)) {
$resultado = "ok";
} else {
$resultado = "ERROR: " . mysqli_error($c);
}
desconectar($c);
return $resultado;
}

function selectAll($a) {
$c = conectar();
$select = "select * from usuario "
. "left join musico on usuario.idusuario=musico.idmusico "
. "left join locales on usuario.idusuario=locales.idlocal "
. "left join fan on usuario.idusuario=fan.idfan "
. "where idusuario=$a";
$resultado = mysqli_query($c, $select);
desconectar($c);
return $resultado;
}

function selectLocales() {
$c = conectar();
$select = "select *, ciudad.nombre as nombreciudad, usuario.nombre from usuario "
. "inner join locales on usuario.idusuario=locales.idlocal "
. "inner join ciudad on usuario.ciudad=ciudad.idciudad "
. "where tipo=1 "
. "order by usuario.ciudad;";
$resultado = mysqli_query($c, $select);
desconectar($c);
return $resultado;
}

function selectMusicos() {
$c = conectar();
$select = "select *, genero.nombre as nombregenero, usuario.nombre as nombremusico from usuario "
. "inner join musico on usuario.idusuario = musico.idmusico "
. "inner join genero on musico.genero = genero.idgenero "
. "where tipo=2 "
. "order by musico.genero";
$resultado = mysqli_query($c, $select);
desconectar($c);
return $resultado;
}

function selectConciertosPropuestos($genero) {
$c = conectar();
$select = "select nombreusuario, idconcierto, usuario.nombre as nombrelocal, ciudad.nombre as nombreciudad, "
. "dia, hora, genero.nombre as nombregenero, concierto.nombre as nombreconcierto from concierto "
. "inner join usuario on concierto.locales=usuario.idusuario "
. "inner join ciudad on usuario.ciudad=ciudad.idciudad "
. "inner join genero on concierto.genero=genero.idgenero "
. "where concierto.genero=$genero "
. "order by dia";
$resultado = mysqli_query($c, $select);
desconectar($c);
return $resultado;
}

function selectConciertosLocal($locales) {
    $c = conectar();
    $select = "select nombreusuario, idconcierto, usuario.nombre as nombrelocal, ciudad.nombre as nombreciudad, "
    . "dia, hora, genero.nombre as nombregenero, concierto.nombre as nombreconcierto from concierto "
    . "inner join usuario on concierto.locales=usuario.idusuario "
    . "inner join ciudad on usuario.ciudad=ciudad.idciudad "
    . "inner join genero on concierto.genero=genero.idgenero "
    . "where concierto.locales=$locales "
    . "order by dia";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
    }

function selectConciertosAprobados() {
$c = conectar();
$select = "select concierto.nombre as nombreconcierto, concierto.idconcierto, nombreartistico, usuario.nombre as nombrelocal, dia, hora, ciudad.nombre as nombreciudad, genero.nombre as nombregenero from concierto
inner join usuario on concierto.locales = usuario.idusuario 
inner join musico on concierto.musico = musico.idmusico 
inner join ciudad on usuario.ciudad = ciudad.idciudad 
inner join genero on musico.genero = genero.idgenero 
where concierto.estado=2";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

function Apuntarse($concierto, $idmusico) {
    $c = conectar();
    $select = "select * from propuesta where concierto = $concierto and musico = $idmusico";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    if (mysqli_num_rows($resultado) == 0) {
        return false;
    }
    return true;
}

function RechazarMusico($concierto, $idmusico) {
    $c = conectar();
    $select = "select * from concierto where idconcierto = $concierto and musico = $idmusico";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    if (mysqli_num_rows($resultado) == 0) {
        return false;
    }
    return true;
}




function altaConciertoMusico($idconcierto, $idmusico) {
    $c = conectar();
    $insert = "insert into propuesta values ($idconcierto, $idmusico, 0)";
    if (mysqli_query($c, $insert)) {
        $resultado = true;
    } else {
        $resultado = mysqli_error($c);
    }
    desconectar($c);
    return $resultado;
}

function bajaConciertoMusico($idconcierto, $idmusico) {
    $c = conectar();
    $insert = "delete from propuesta where concierto = $idconcierto and musico = $idmusico";
    if (mysqli_query($c, $insert)) {
        $resultado = true;
    } else {
        $resultado = mysqli_error($c);
    }
    desconectar($c);
    return $resultado;
}

function verMusicos($idconcierto) {
    $c = conectar();
    $select = "select * from usuario
inner join propuesta on usuario.idusuario = propuesta.musico
inner join musico on propuesta.musico = musico.idmusico
where concierto = $idconcierto";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

function Aprobar($idmusico, $idconcierto) {
    $c = conectar();
    mysqli_query($c, "update concierto set musico = $idmusico where idconcierto = $idconcierto;");
    mysqli_query($c, "update propuesta set estado = 2 where idconcierto = $idconcierto;");
    mysqli_query($c, "update concierto set estado = 2 where idconcierto = $idconcierto;");

    desconectar($c);
}

function Rechazar($idconcierto) {
    $c = conectar();
    mysqli_query($c, "update concierto set musico = null where idconcierto = $idconcierto;");
    mysqli_query($c, "update propuesta set estado = 1 where idconcierto = $idconcierto;");
    mysqli_query($c, "update concierto set estado = 1 where idconcierto = $idconcierto;");
    desconectar($c);
}

function votarConcierto($idconcierto, $idusuario){
    $c = conectar();
    mysqli_query($c, "insert into voto_concierto values(Null, $idusuario, $idconcierto);");
    desconectar($c);
}
function quitarVotoConcierto($con, $fan){
    $c = conectar();
    mysqli_query($c, "delete from voto_concierto where fan = $fan and concierto = $con;");
    desconectar($c);
}

function votarMusico($idmusico, $idusuario){
    $c = conectar();
    mysqli_query($c, "insert into voto_musico values(Null, $idusuario, $idmusico);");
    desconectar($c);
}
function quitarVotoMusico($mus, $fan){
    $c = conectar();
    mysqli_query($c, "delete from voto_musico where fan = $fan and musico = $mus;");
    desconectar($c);
}

function votosConcierto($idconcierto){
    $c = conectar();
    $select = "select * from voto_concierto where concierto = $idconcierto";
    $result = mysqli_query($c, $select);
    desconectar($c);
    $array = [];
    while($fila = mysqli_fetch_assoc($result)){
        $array[] = $fila;
    }
    return count($array);
}

function votosMusico($idmusico){
    $c = conectar();
    $select = "select * from voto_musico where musico = $idmusico";
    $result = mysqli_query($c, $select);
    desconectar($c);
    $array = [];
    while($fila = mysqli_fetch_assoc($result)){
        $array[] = $fila;
    }
    return count($array);
}

function haVotadoConcierto($id,$con){
    $c = conectar();
    $select = "select * from concierto 
    inner join voto_concierto on voto_concierto.concierto=concierto.idconcierto 
    inner join usuario on voto_concierto.fan=usuario.idusuario
    where voto_concierto.fan=$id and voto_concierto.concierto=$con;";
    $result = mysqli_query($c, $select);
    desconectar($c);
    if (mysqli_num_rows($result) == 0) {
        return false;
    }
    return true;
}

function haVotadoMusico($id,$mus){
    $c = conectar();
    $select = "select * from musico 
    inner join voto_musico on voto_musico.musico=musico.idmusico 
    inner join usuario on voto_musico.fan=usuario.idusuario
    where voto_musico.fan=$id and voto_musico.musico=$mus;";
    $result = mysqli_query($c, $select);
    desconectar($c);
    if (mysqli_num_rows($result) == 0) {
        return false;
    }
    return true;
}