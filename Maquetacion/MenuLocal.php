<html>
    <head>
        <meta charset="UTF-8">
        <title>Local</title>
        <link href="styles/MenuLocal.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php
        session_start();
        if (isset($_SESSION["tipo"]) && $_SESSION["tipo"] == 1) {
            ?>
            <form>
                <input type="button" onclick="location.href = 'PerfilLocal.php'" value="Ver datos de perfil" name="modificar">
                
            </form>
            <div class="tabla">
                <p class="text">Conciertos creados</p>
                <?php
                require_once 'bbdd.php';
                $conciertos = selectAllConciertos();
                $fila = mysqli_fetch_assoc($conciertos);
                $musico = $fila["musico"];
                $locales = $fila["locales"];
                $dia = $fila["dia"];
                ?>
                <table class="tablaConcierto">
                    <tr>
                        <th>Nombre</th>
                        <th>Día</th>
                        <th>Hora</th>
                        <th>Género</th>
                        <th>Gestionar</th>
                    </tr>
                    <?php
                    $concierto = selectConciertosLocal($_SESSION["idusuario"]);
                    while ($fila = mysqli_fetch_assoc($concierto)) {
                        echo"<tr><td>" . $fila["nombreconcierto"] . "</td>";
                        echo"<td>" . $fila["dia"] . "</td>";
                        echo"<td>" . $fila["hora"] . "</td>";
                        echo"<td>" . $fila["nombregenero"] . "</td>";
                        echo"<td>";
                        $select = verMusicos($fila["idconcierto"]);
                        echo"<form action='' method='post'>";
                        echo "<a href = 'vermusicos.php?idconcierto=".$fila["idconcierto"]."'>Ver Musicos</a>";
                        echo"</td></tr>";
                    }
                    ?>
                </table>
                <?php
                if (isset($_POST["vermusicos"])) {
                    $insert = verMusicos($_SESSION["idusuario"]);
                }
                ?>
            </div>
            <div class="crear">
                <p class="text">Crear un concierto</p> 
                <form method="POST">
                    <p>Nombre: <input type="text" name="nombre"></p> 
                    <p>Fecha del concierto: <input type="date" name="fecha"></p> 
                    <p>Hora del concierto: <input type="time" name="hora"></p> 
                    <p>Valor económico: <input type="number" name="valor"></p> 
                    <p>Género: <select name="genero">
                            <?php
                            require_once 'bbdd.php';
                            $genero = selectGenero();
                            while ($fila = mysqli_fetch_assoc($genero)) {
                                echo "<option value = '" . $fila["idgenero"] . "'>" . $fila["nombre"] . "</option>";
                            }
                            ?>
                        </select></p>
                    <input type="submit" value="Crear" name="crear">
                </form> 
                <?php
                require_once("bbdd.php");
                if (isset($_POST["crear"])) {
                    $nombre = $_POST["nombre"];
                    $dia = $_POST["fecha"];
                    $hora = $_POST["hora"];
                    $pago = $_POST["valor"];
                    $genero = $_POST["genero"];
                    $idusuario = $_SESSION["idusuario"];
                    $resultat = insertarConcierto($nombre, $dia, $hora, $pago, $idusuario, $genero);
                    if ($resultat == "ok") {
                        echo "Concierto registrado correctamente<br>";
                    } else {
                        echo "Error: .$resultat<br>";
                    }
                }
                ?>
            </div>
            <div class="borrar">
                <p class="text">Borrar un concierto</p> 
                <form method="POST">
                    <p>Nombre Concierto a borrar: <select name="id">
                            <?php
                            require_once 'bbdd.php';
                            $nombre = selectConciertosLocal($_SESSION["idusuario"]);
                            while ($fila = mysqli_fetch_assoc($nombre)) {
                                echo "<option value=".$fila["idconcierto"].">" . $fila["nombreconcierto"] . "</option>";
                            }
                            ?>
                        </select></p>
                    <input type="submit" value="Borrar" name="borrar">
                </form> 
                <?php
                require_once("bbdd.php");

                if (isset($_POST["borrar"])) {
                    $id = $_POST["id"];
                    $resultado = eliminarConcierto($id);
                    if ($resultado == "ok") {
                        echo "Concierto eliminado correctamente<br>";
                    } else {
                        echo "Error: .$resultado<br>";
                    }
                }
                ?>
            </div>
            <form action="Homepage.php" method="POST">
                <a href="logout.php">Logout</a>
            </form>  
            <?php
        } else {
            echo "No tienes permiso para ver esta pagina<br>";
        }
        ?>
    </body>
</html>
