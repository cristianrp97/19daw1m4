<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link href="styles/MenuMusico.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php
        require_once 'bbdd.php';
        session_start();
        if (isset($_SESSION["tipo"]) && $_SESSION["tipo"] == 2) {
            ?>
            <div>
                <input type="button" onclick="location.href = 'PerfilMusico.php'" value="Ver datos de perfil" name="modificar">
            </div>
            <div class="tab">
                <p class="text">Conciertos propuestos</p><br>
                <table class="tablaMusico">
                    <tr>
                        <th>Nombre del concierto</th>
                        <th>Nombre del local</th>
                        <th>Ciudad</th>
                        <th>Fecha</th>
                        <th>Hora</th>
                        <th>Género</th>
                        <th>Opción de inscripción</th>
                    </tr>
                    <?php
                    // $genero debe tener el idgenero del musico logueado
                    $conciertos = selectConciertosPropuestos($_SESSION["genero"]);
                    while ($fila = mysqli_fetch_assoc($conciertos)) {
                        echo"<tr><td>" . $fila["nombreconcierto"] . "</td>";
                        echo"<td>" . $fila["nombrelocal"] . "</td>";
                        echo"<td>" . $fila["nombreciudad"] . "</td>";
                        echo"<td>" . $fila["dia"] . "</td>";
                        echo"<td>" . $fila["hora"] . "</td>";
                        echo"<td>" . $fila["nombregenero"] . "</td>";
                        echo"<td>";
                        $select = Apuntarse($fila["idconcierto"], $_SESSION["idusuario"]);
                        if (!$select) {
                            echo"<form action='' method='post'>
                                 <input type='hidden' name='concierto' value='" . $fila["idconcierto"] . "'>
                                 <input type='submit' name='apuntarse' value='Apuntarse'></form>";
                        } else {
                            echo"<form action='' method='post'>
                                 <input type='hidden' name='concierto' value='" . $fila["idconcierto"] . "'>
                                 <input type='submit' name='desapuntarse' value='Desapuntarse'></form>";
                        }
                        echo"</td>";
                        echo"</tr>";
                    }
                    ?>
                </table>
                <?php
                if (isset($_POST["apuntarse"])) {
                    $insert = altaConciertoMusico($_POST["concierto"], $_SESSION["idusuario"]);
                }
                if (isset($_POST["desapuntarse"])) {
                    $insert = bajaConciertoMusico($_POST["concierto"], $_SESSION["idusuario"]);
                }
                ?>
            </div>
            <div><br><br>
                <form action="Homepage.php" method="POST">
                    <a href="logout.php">Logout</a>
                </form>  
            </div>
            <?php
        } else {
            echo "No tienes permiso para ver esta pagina<br>";
        }
        ?>
    </body>
</html>