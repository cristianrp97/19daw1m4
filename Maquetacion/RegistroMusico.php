
    <h1 > Registrar un músico</h1>
    <form action="" method="POST">
        <p>Género musical: <select name="genero">
                <?php
                require_once 'bbdd.php';
                $genero = selectGenero();
                while ($fila = mysqli_fetch_assoc($genero)) {
                    echo "<option value = '" . $fila["idgenero"] . "'>" . $fila["nombre"] . "</option>";
                }
                ?>
            </select></p>
        <p>Nombre: <input type="text" name="nombre"</p><br>
        <p>Apellidos: <input type="text" name="apellidos"</p><br>
        <p>Ciudad: <select name="ciudad">
                <?php
                require_once 'bbdd.php';
                $ciudad = selectCiudad();
                while ($fila = mysqli_fetch_assoc($ciudad)) {
                    echo "<option value = '" . $fila["idciudad"] . "'>" . $fila["nombre"] . "</option>";
                }
                ?>
            </select></p>
        <p>Teléfono: <input type="number" name="telefono"</p><br>
        <p>Email: <input type="text" name="email"</p><br>
        <p>Web: <input type="text" name="web"</p><br>
        <p>Nombre artístico: <input type="text" name="artistico"</p><br>
        <p>Nº componentes del grupo: <input type="text" name="numcomponentes"</p><br>
        <p>Nombre usuario: <input type="text" name="nombreusuario"</p><br>
        <p>Contraseña: <input type="password" name="pass"</p><br><br>

        <button onclick="RegistroMusico()" type="submit" value="Registrar" name="registrar">Registrar</button> 
        
    </form>
    <?php
    if (isset($_POST["registrar"])) {
        $genero = $_POST["genero"];
        $nombre = $_POST["nombre"];
        $apellidos = $_POST["apellidos"];
        $ciudad = $_POST["ciudad"];
        $telefono = $_POST["telefono"];
        $email = $_POST["email"];
        $web = $_POST["web"];
        $artistico = $_POST["artistico"];
        $numcomponentes = $_POST["numcomponentes"];
        $nombreusuario = $_POST["nombreusuario"];
        $pass = $_POST["pass"];
        $tipo = 2;
        $resultado = registrarMusico($nombreusuario, $pass, $nombre, $genero, $web, $artistico, $numcomponentes, $email, $telefono, $ciudad, $tipo);
        if ($resultado != "ok") {
            echo "Error: .$resultado<br>";
        } 
    }
    ?>
