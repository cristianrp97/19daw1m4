<html>
    <head>
        <meta charset="UTF-8">
        <title>Modificar Perfil</title>
        <link href="styles/Homepage.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php
        session_start();
        if (isset($_SESSION["tipo"])) {
            require_once 'bbdd.php';
            ?>
            <?php
            $dato = selectAll($_SESSION["idusuario"]);
            $fila = mysqli_fetch_assoc($dato);
            ?>
            <h1>Modificar Perfil</h1>
            <form method="POST">
                <p>Nombre: <input type="text" name="modNombre" value="<?php echo $fila["nombre"]; ?>"></p>
                <p>Email: <input type="text" name="modEmail" value="<?php echo $fila["email"]; ?>"></p>
                <p>Teléfono: <input type="text" name="modTel" value="<?php echo $fila["telefono"]; ?>"></p>
                <?php
                if ($_SESSION["tipo"] == 1) {
                    ?>
                    <p>Dirección: <input type="text" name="modDir" value="<?php echo $fila["direccion"]; ?>"></p>
                    <p>Aforo: <input type="number" name="modAforo" value="<?php echo $fila["aforo"]; ?>"></p>
                    <?php
                } else if ($_SESSION["tipo"] == 2) {
                    ?>
                    <p>Web: <input type="text" name="modWeb" value="<?php echo $fila["web"]; ?>"></p>
                    <p>Número de componetes: <input type="number" name="modComponentes" value="<?php echo $fila["numcomponentes"]; ?>"></p>
                    <?php
                } else if ($_SESSION["tipo"] == 3) {
                    ?>
                    <p>Apellidos: <input type="text" name="modApe" value="<?php echo $fila["apellidos"]; ?>"></p>
                    <p>Sexo:
                        <label for="hombre">Hombre</label>
                        <input id="hombre" type="radio" name="modSexo" value="hombre" required/>
                        <label for="mujer">Mujer</label>
                        <input id="mujer" type="radio" name="modSexo" value="mujer" required/>
                    </p>
                    <?php
                }
                ?>
                <input type="hidden" name="nombreusuario" value="<?php echo $fila["nombreusuario"] ?>">
                <input type="submit" name="modificar" value="Modificar">
                <input type="button" onclick="location.href = 'Homepage.php'" value="Volver" name="volver">
            </form>
            <?php
            if (isset($_POST["modificar"])) {
                $nombreusuario = $_POST["nombreusuario"];
                $tipo = $_SESSION["tipo"];
                $nombre = $_POST["modNombre"];
                $email = $_POST["modEmail"];
                $telefono = $_POST["modTel"];

                if ($tipo == 1) {
                    $direccion = $_POST["modDir"];
                    $aforo = $_POST["modAforo"];
                    $resultado = modLocal($nombreusuario, $nombre, $email, $telefono, $direccion, $aforo);
                } else if ($tipo == 2) {
                    $web = $_POST["modWeb"];
                    $numcomponentes = $_POST["modComponentes"];
                    $resultado = modMusico($nombreusuario, $nombre, $email, $telefono, $web, $numcomponentes);
                } else if ($tipo == 3) {
                    $apellidos = $_POST["modApe"];
                    $sexo = $_POST["modSexo"];
                    $resultado = modFan($nombreusuario, $nombre, $apellidos, $email, $telefono, $sexo);
                }
            }
        } else {
            ?>
            <script>
                alert('Acceso denegado');
                location.href = "Homepage.php";
            </script>
            <?php
        }
        ?>
    </div>
</body>
</html>

