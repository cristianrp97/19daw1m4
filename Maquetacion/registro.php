<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registro</title>
    <script src="alerts.js"></script>

    <style>
            body{
                background-image: url("styles/fondo.png");
                background-size: cover;
            }
            .user{
                height: 275px;
             }
             #buttonregistros{
                width:100%;
            }
            #buttonregistros>button{
                width:33%;
                height:60px;
                font-weight:bold;
                color:white;
                background-color:#0291ff;
                border-color:#0291ff;
            }
            #buttonregistros>button:hover{
                background-color:white;
                color: #0291ff;
            }
        
            #buttonvolver{
                position:absolute;
                bottom:20px;
                right: 20px;
                width:90px;
                height:45px;
            }
        </style>
</head>
<body>
    <div id="buttonregistros">
    <button onclick="window.location.href='registro.php?tipo=local'">LOCAL</button>
    <button onclick="window.location.href='registro.php?tipo=musico'">MÚSICO</button>
    <button onclick="window.location.href='registro.php?tipo=fan'">FAN</button>
    </div>
   


    <?php
    if (isset($_GET["tipo"])){
        if($_GET["tipo"]=="local"){
            include_once 'RegistroLocal.php';
        }
        if($_GET["tipo"]=="musico"){
            include_once 'RegistroMusico.php';
        }
        if($_GET["tipo"]=="fan"){
            include_once 'RegistroFan.php';
        }
    }
    ?>
     <p><input id="buttonvolver" type="button" onclick="location.href = 'Homepage.php'" value="Volver" name="volver"></p>
</body>
</html>