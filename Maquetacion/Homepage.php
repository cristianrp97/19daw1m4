<html>
    <head>
        <meta charset="UTF-8">
        <link href="styles/Homepage.css" rel="stylesheet" type="text/css"/>
        <title></title>
    </head>
    <body>
        <?php
        require_once 'bbdd.php';
        session_start();
        if (isset($_POST["logout"])) {
            session_destroy();
            echo "Se ha cerrado la sesion<br><br>";
        }
        ?>
        <div>
            <div class="top">
                <div class="idioma">
                    <p class="language">Idioma</p>
                    <select>
                        <option>Castellano</option>
                        <option>Catalán</option>
                        <option>Inglés</option>
                    </select>
                </div>
                <div class="buscador">
                    <img class="lupa" src="lupa.png" alt=""/>
                    <input class="buscador2" type="name" name="buscador">
                </div>
                <div class="loginRegist">
                    <div class="login">
                    <?php
                    if (isset($_SESSION["tipo"])) {
                        if ($_SESSION["tipo"] == 1) {
                            header("Location: MenuLocal.php");
                        } else if ($_SESSION["tipo"] == 2) {
                            header("Location: MenuMusico.php");
                        } else if ($_SESSION["tipo"] == 3) {
                            header("Location: MenuFan.php");
                        }
                    }
                    ?>
                    <form action="Homepage.php" method="POST">
                        <p>Nombre de usuario: <input type="name" name="usuario"></p>
                        <p>Contraseña: <input type="password" name="password"></p>
                        <input type="submit" value="Login" name="login">
                    </form>
                    <?php
                    require_once 'bbdd.php';
                    if (isset($_POST["login"])) {
                        $usuario = $_POST["usuario"];
                        $password = $_POST["password"];
                        $verificar = verificar($usuario, $password);
                        if ($verificar) {
                            $resultado = login($usuario, $password);
                            $_SESSION["idusuario"] = $resultado["idusuario"];
                            $_SESSION["tipo"] = $resultado["tipo"];
                            if($resultado["tipo"] == 2){
                                $_SESSION["genero"] = $resultado["genero"];
                            }
                            header("Location: Homepage.php");
                        } else {
                            echo "Usuario o contraseña incorrecto";
                        }
                    }
                    ?>
                </div>
                <div class="registrarse">
                    <form>
                        <p>Si no tienes una cuenta regístrate!</p>
                        <input type="button" onclick="location.href = 'registro.php'" value="Registrarse" name="registrar">
                    </form>
                </div>
            </div>
            </div>
            <div class="principal">
                <div class="logo">
                    <img class="florida" src="FloridaRecordsTransparente.png" alt=""/>
                </div>
                <div class="titulo">
                    <p class="homepage">Florida Records</p>
                </div>
            </div>
            <div class="tablas">
                <div class="conciertos">
                    <table class="tablaConcierto">
                        <tr><th colspan="3">Locales</th></tr>
                        <tr><th>Nombre</th><th>Ciudad</th></tr>
                        <?php
                        $locales = selectLocales();
                        while ($fila = mysqli_fetch_assoc($locales)) {
                            echo"<tr><td>" . $fila["nombre"] . "</td>";
                            echo"<td>" . $fila["nombreciudad"] . "</td></tr>";
                        }
                        ?>
                    </table>
                </div>
                <div class="musicos">
                    <table class="tablaMusicos">
                        <tr><th colspan="4">Listado de músicos</th></tr>
                        <tr><th>Nombre Artístico</th>
                        <th>Genero</th>
                        <th>Votos</th></tr>
                        <?php
                        $musicos = selectMusicos();
                        while ($fila = mysqli_fetch_assoc($musicos)) {
                            echo"<tr><td>" . $fila["nombreartistico"] . "</td>";
                            echo"<td>" . $fila["nombregenero"] . "</td>";
                            $votos = votosMusico($fila["idmusico"]);
                            echo"<td>$votos</td></tr>";
                        }
                        ?>
                    </table>
                </div>
                <div class="conciertos">
                <table class="tablaMusicos">
                    <tr><th colspan="8">Conciertos aprobados</th></tr>
                    <th>Nombre Concierto</th>
                    <th>Nombre Músico</th>
                    <th>Nombre Local</th>
                    <th>Día</th>
                    <th>Hora</th>
                    <th>Ciudad</th>
                    <th>Género</th>
                    <th>Votos</th>
                    <?php
                    $aprobados = selectConciertosAprobados();
                    while ($fila = mysqli_fetch_assoc($aprobados)) {
                        echo"<tr><td>" . $fila["nombreconcierto"] . "</td>";
                        echo"<td>" . $fila["nombreartistico"] . "</td>";
                        echo"<td>" . $fila["nombrelocal"] . "</td>";
                        echo"<td>" . $fila["dia"] . "</td>";
                        echo"<td>" . $fila["hora"] . "</td>";
                        echo"<td>" . $fila["nombreciudad"] . "</td>";
                        echo"<td>" . $fila["nombregenero"] . "</td>";
                        $votos = votosConcierto($fila["idconcierto"]);
                        echo"<td>$votos</td></tr>";
                    }
                    ?>
                </table>
            </div>
            </div>
        </div>
        <footer>
            <div class="banner">
                <div class="d1">
                    <p class="tituloF">INFORMACIÓN</p>
                    <p class="info">Proyecto Transversal</p>
                    <p class="info">Grupo 4</p>
                    <p class="info">© Copyright 2019 | Aviso legal</p>
                </div>
                <div class="d1">
                    <p class="tituloF">REDES SOCIALES</p>
                    <div class="apps2"><img class="imgApps" src="twitter.png" alt=""/></div>
                    <div class="apps2"><img class="imgApps" src="facebook.png" alt=""/></div>
                    <div class="apps2"><img class="imgApps" src="insta.png" alt=""/></div>
                    <p class="info">@FloridaRecords</p>
                </div>
                <div class="d1">
                    <p class="tituloF">CONTACTO</p>
                    <p class="info">Telf: + (34)932 2222 085</p>
                    <p class="info">Email: floridarecords@florida.com</p>
                </div>
            </div>
        </footer>
    </div>
</body>
</html>
