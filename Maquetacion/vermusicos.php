<html>
    <head>
        <meta charset="UTF-8">
        <link href="styles/MenuMusico.css" rel="stylesheet" type="text/css"/>
        <title>Ver músicos</title>
    </head>
    <body>
        <?php
        require_once 'bbdd.php';
        session_start();
        if (isset($_SESSION["tipo"]) && $_SESSION["tipo"] == 1) {
            ?>
            <div class="tabla">
                <p class="text">Músicos apuntados</p>
                <?php
                require_once 'bbdd.php';
                ?>
                <table class="tablaMusicos">
                    <tr>
                        <th>Nombre</th>
                        <th>Gestionar</th>
                    </tr>
                    <?php
                    $musicologo = verMusicos($_GET["idconcierto"]);
                    while ($fila = mysqli_fetch_assoc($musicologo)) {
                        echo"<tr><td>" . $fila["nombreartistico"] . "</td>";
                        echo"<td>";
                        $select = RechazarMusico($_GET["idconcierto"], $fila["idusuario"]);
                        if (!$select) {
                            echo"<form action='' method='get'>
                                 <input type='hidden' name='idmusico' value='" . $fila["idusuario"] . "'>
                                 <input type='hidden' name='idconcierto' value='" . $_GET["idconcierto"] . "'>
                                 <input type='submit' name='aprobar' value='Aprobar'></form>";
                        }else{
                            echo"<form action='' method='get'>
                                 <input type='hidden' name='idconcierto' value='" . $_GET["idconcierto"] . "'>
                                 <input type='submit' name='rechazar' value='Rechazar'></form>";
                        }
                        echo"</td>";
                        echo"</tr>";
                    }
                    ?>
                </table>
                <?php
                if (isset($_GET["aprobar"])) {
                    Aprobar($_GET["idmusico"],$_GET["idconcierto"]);
                }
                if(isset($_GET["rechazar"])){
                    Rechazar($_GET["idconcierto"]);
                }
                ?>
            </div>
            <div><br><br>
                <form action="Homepage.php" method="POST">
                    <a href="logout.php">Logout</a>
                </form>  
            </div>
            <?php
        } else {
            echo "No tienes permiso para ver esta pagina<br>";
        }
        ?>
    </body>
</html>
