<html>
    <head>
        <meta charset="UTF-8">
        <link href="styles/PerfilLocal.css" rel="stylesheet" type="text/css"/>
        <title></title>
    </head>
    <body>
        <?php
        session_start();
        if (isset($_SESSION["tipo"]) && $_SESSION["tipo"] == 1) {
            ?>

            <div>
                <h2>INFORMACIÓN DEL PERFIL (LOCAL)</h2>
                <div class="div1">
                    <?php
                    require_once 'bbdd.php';
                    $locales = selectAllLocales($_SESSION["idusuario"]);
                    $fila = mysqli_fetch_assoc($locales);
                    $nombre = $fila["nombre"];
                    $direccion = $fila["direccion"];
                    $aforo = $fila["aforo"];
                    $telefono = $fila["telefono"];
                    $email = $fila["email"];
                    $nombreusuario = $fila["nombreusuario"];
                    ?>
                    <table style="width:100%; height: 100%;">
                        <tr>
                            <th>INFORMACIÓN GENERAL</th>
                        </tr>
                        <tr>
                            <td>Nombre:
                                <?php
                                echo $nombre;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Ubicación:
                                <?php
                                echo $direccion;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Aforo:
                                <?php
                                echo $aforo;
                                ?>
                            </td>                    
                        </tr>
                    </table>
                </div><div class="divimg">
                    <img style="width: 300px; height: 300px;"src="user.jpg" alt=""/>
                </div>

            </div>
            <div>
                <div class="div2">
                    <table style="width:100%; height: 100%;">
                        <tr>
                            <th>INFORMACIÓN ADICIONAL</th>
                        </tr>
                        <tr>
                            <td>Telefono:
                                <?php
                                echo $telefono;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Email:
                                <?php
                                echo $email;
                                ?>
                            </td>
                        </tr>
                    </table>
                </div><div class="div3">
                    <table style="width:100%; height: 100%;">
                        <tr>
                            <th>Nombre de Usuario:
                                <?php
                                echo $nombreusuario;
                                ?>
                            </th>
                        </tr>
                    </table>
                </div>
            </div>
            <input type="button" onclick="location.href = 'modPerfil.php'" value="Modificar perfil" name="modPerfil">
            <input type="button" onclick="location.href = 'MenuLocal.php'" value="Volver" name="volver">
            <form action="Homepage.php" method="POST">
                <a href="logout.php">Logout</a>
            </form>  
            <?php
        } else {
            echo "No tienes permiso para ver esta pagina<br>";
        }
        ?>
    </body>
</html>
