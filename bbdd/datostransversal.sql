CREATE DATABASE  IF NOT EXISTS `proyectotransversal` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `proyectotransversal`;
-- MySQL dump 10.16  Distrib 10.1.33-MariaDB, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: proyectotransversal
-- ------------------------------------------------------
-- Server version	10.1.33-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ciudad`
--

DROP TABLE IF EXISTS `ciudad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ciudad` (
  `idciudad` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `provincia` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idciudad`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ciudad`
--

LOCK TABLES `ciudad` WRITE;
/*!40000 ALTER TABLE `ciudad` DISABLE KEYS */;
INSERT INTO `ciudad` VALUES (1,'Barcelona','Barcelona'),(2,'Madrid','Madrid'),(3,'Sevilla','Sevilla'),(4,'Valencia','Valencia'),(5,'Malaga','Malaga');
/*!40000 ALTER TABLE `ciudad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `concierto`
--

DROP TABLE IF EXISTS `concierto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `concierto` (
  `idconcierto` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `estado` int(1) NOT NULL,
  `dia` date NOT NULL,
  `hora` time NOT NULL,
  `pago` decimal(6,2) NOT NULL,
  `locales` int(11) NOT NULL,
  `genero` int(11) NOT NULL,
  `musico` int(11) DEFAULT NULL,
  PRIMARY KEY (`idconcierto`),
  KEY `FK_locales` (`locales`),
  KEY `FK2_genero` (`genero`),
  KEY `FK_musico` (`musico`),
  CONSTRAINT `FK2_genero` FOREIGN KEY (`genero`) REFERENCES `genero` (`idgenero`),
  CONSTRAINT `FK_locales` FOREIGN KEY (`locales`) REFERENCES `locales` (`idlocal`),
  CONSTRAINT `FK_musico` FOREIGN KEY (`musico`) REFERENCES `musico` (`idmusico`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `concierto`
--

LOCK TABLES `concierto` WRITE;
/*!40000 ALTER TABLE `concierto` DISABLE KEYS */;
INSERT INTO `concierto` VALUES (1,'TomorrowLand',1,'2019-06-27','22:00:00',500.00,3,2,NULL),(2,'Florida',1,'2019-06-30','20:30:00',40.00,3,1,NULL),(3,'BBF',1,'2019-07-12','22:15:00',45.00,3,6,NULL);
/*!40000 ALTER TABLE `concierto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fan`
--

DROP TABLE IF EXISTS `fan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fan` (
  `idfan` int(11) NOT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `sexo` varchar(1) DEFAULT NULL,
  `nacimiento` date NOT NULL,
  PRIMARY KEY (`idfan`),
  CONSTRAINT `FK_idfan` FOREIGN KEY (`idfan`) REFERENCES `usuario` (`idusuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fan`
--

LOCK TABLES `fan` WRITE;
/*!40000 ALTER TABLE `fan` DISABLE KEYS */;
INSERT INTO `fan` VALUES (7,'Rodriguez Padilla','m','1997-01-12'),(8,'Asenjo Nora','m','2019-05-29'),(9,'Muntanyola ','m','2019-05-27');
/*!40000 ALTER TABLE `fan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genero`
--

DROP TABLE IF EXISTS `genero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genero` (
  `idgenero` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  PRIMARY KEY (`idgenero`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genero`
--

LOCK TABLES `genero` WRITE;
/*!40000 ALTER TABLE `genero` DISABLE KEYS */;
INSERT INTO `genero` VALUES (1,'Rap'),(2,'Techno'),(3,'Rock'),(4,'Blues'),(5,'Country'),(6,'Hip-Hop'),(7,'Heavy Metal'),(8,'Reggaeton');
/*!40000 ALTER TABLE `genero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locales`
--

DROP TABLE IF EXISTS `locales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locales` (
  `idlocal` int(11) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `aforo` int(11) NOT NULL,
  `imagen` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idlocal`),
  CONSTRAINT `FK_idlocal` FOREIGN KEY (`idlocal`) REFERENCES `usuario` (`idusuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locales`
--

LOCK TABLES `locales` WRITE;
/*!40000 ALTER TABLE `locales` DISABLE KEYS */;
INSERT INTO `locales` VALUES (1,'calle Pelayo',300,NULL),(2,'calle Pelayo',2000,NULL),(3,'ParalÂ·lel',500,NULL);
/*!40000 ALTER TABLE `locales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `musico`
--

DROP TABLE IF EXISTS `musico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `musico` (
  `idmusico` int(11) NOT NULL,
  `nombreartistico`varchar(200) NOT NULL,
  `genero` int(11) NOT NULL,
  `web` varchar(200) DEFAULT NULL,
  `numcomponentes` int(11) NOT NULL,
  PRIMARY KEY (`idmusico`),
  KEY `FK_genero` (`genero`),
  CONSTRAINT `FK_genero` FOREIGN KEY (`genero`) REFERENCES `genero` (`idgenero`),
  CONSTRAINT `FK_idmusico` FOREIGN KEY (`idmusico`) REFERENCES `usuario` (`idusuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `musico`
--

LOCK TABLES `musico` WRITE;
/*!40000 ALTER TABLE `musico` DISABLE KEYS */;
INSERT INTO `musico` VALUES (4,'Carlcox',2,'www.carlcox.com',1);
/*!40000 ALTER TABLE `musico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `propuesta`
--

DROP TABLE IF EXISTS `propuesta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `propuesta` (
  `concierto` int(11) NOT NULL,
  `musico` int(11) NOT NULL,
  `estado` int(1) NOT NULL,
  PRIMARY KEY (`concierto`,`musico`),
  KEY `FK3_musico` (`musico`),
  CONSTRAINT `FK2_concierto` FOREIGN KEY (`concierto`) REFERENCES `concierto` (`idconcierto`),
  CONSTRAINT `FK3_musico` FOREIGN KEY (`musico`) REFERENCES `musico` (`idmusico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `propuesta`
--

LOCK TABLES `propuesta` WRITE;
/*!40000 ALTER TABLE `propuesta` DISABLE KEYS */;
/*!40000 ALTER TABLE `propuesta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombreusuario` varchar(200) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telefono` varchar(9) DEFAULT NULL,
  `ciudad` int(11) NOT NULL,
  `tipo` int(1) DEFAULT NULL,
  PRIMARY KEY (`idusuario`),
  KEY `FK_ciudad` (`ciudad`),
  CONSTRAINT `FK_ciudad` FOREIGN KEY (`ciudad`) REFERENCES `ciudad` (`idciudad`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'florida','$2y$10$E2nF01gjXDgXKzWA1BAdEuxkY17ECAUmS3pADUWICGQTrbPj0s9IK','Florida','florida@gmail.com','7547547',1,1),(4,'carlcox','$2y$10$JzPFZy/4QPAKla3LFTCMteeFSDJbjRxSQ9sv6SVdQqZmZfFOXFTnG','Carl Cox','carlcox@gmail.com','666666666',2,2),(5,'anuel','$2y$10$ij2m0c2mtdUZ5rWETGUYCuvGoQQmH2OPsi9WHOrYegBzsXRn9id8G','Anuel AA','anuel@gmail.com','666666666',1,2),(6,'zasko','$2y$10$271pVAYOmRLDvIhQfNi/P.HngYgzC1JR8rL4X2J.JiJLH9KQBabay','Zasko Master','zaski@gmail.com','666666666',3,2),(7,'cristian','$2y$10$o9/e9iBVBGaWuU9/by57PO22wHMUcOpC/9EaUA0QzI12UUmZJi9O6','Cristian','cristianrp97@gmail.com','666666666',1,3),(8,'roger','$2y$10$93KPrLySmEatwuvlJd126utiSw.XduIIKPnQ0zesvLS/CTrrRE9i.','Roger','rogerasenjo@gmail.com','666666666',1,3),(9,'pau','$2y$10$Eeq04LwCTxEVTcn5A2mDxOTMvWUyzTGqnKix6Ve/S5Sm2g7cuZ8E.','Pau','paumunta@gmail.com','666666666',1,3);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `voto_concierto`
--

DROP TABLE IF EXISTS `voto_concierto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voto_concierto` (
  `id` int NOT NULL AUTO_INCREMENT,
  `fan` int(11) NOT NULL,
  `concierto` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_concierto` (`concierto`),
  CONSTRAINT `FK2_fan` FOREIGN KEY (`fan`) REFERENCES `fan` (`idfan`),
  CONSTRAINT `FK_concierto` FOREIGN KEY (`concierto`) REFERENCES `concierto` (`idconcierto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `voto_concierto`
--

LOCK TABLES `voto_concierto` WRITE;
/*!40000 ALTER TABLE `voto_concierto` DISABLE KEYS */;
/*!40000 ALTER TABLE `voto_concierto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `voto_musico`
--

DROP TABLE IF EXISTS `voto_musico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voto_musico` (
  `id` int NOT NULL AUTO_INCREMENT,
  `fan` int(11) NOT NULL,
  `musico` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK2_musico` (`musico`),
  CONSTRAINT `FK2_musico` FOREIGN KEY (`musico`) REFERENCES `musico` (`idmusico`),
  CONSTRAINT `FK_fan` FOREIGN KEY (`fan`) REFERENCES `fan` (`idfan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `voto_musico`
--

LOCK TABLES `voto_musico` WRITE;
/*!40000 ALTER TABLE `voto_musico` DISABLE KEYS */;
/*!40000 ALTER TABLE `voto_musico` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-26 12:36:28
