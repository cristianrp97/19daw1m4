drop database if exists proyectoTransversal;
create database proyectoTransversal;
use proyectoTransversal;

create table Genero(
	idGenero INT(11) NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(60) NOT NULL,
    CONSTRAINT PK_idGenero PRIMARY KEY (idGenero)
);

create table Ciudad(
	idCiudad INT(11) NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(100) NOT NULL,
    provincia VARCHAR(100),
    CONSTRAINT PK_idCiudad PRIMARY KEY (idCiudad)
);

create table Usuario(
	idUsuario INT(11) NOT NULL AUTO_INCREMENT,
    nombreUsuario VARCHAR (200) NOT NULL,
    pass VARCHAR(255) NOT NULL,
    nombre VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL,
    telefono VARCHAR(9),
    ciudad INT(11)NOT NULL,
    tipo INT(1),
    constraint PK_idUsuario primary key (idUsuario),
    CONSTRAINT FK_ciudad FOREIGN KEY (ciudad) REFERENCES Ciudad(idCiudad)
);

create table Locales(
	idLocal INT (11) NOT NULL,
    direccion VARCHAR (200) NOT NULL,
    aforo INT (11) NOT NULL,
    imagen VARCHAR(200),
    constraint PK_idlocal primary key (idLocal),
    constraint FK_idLocal foreign key (idLocal) references Usuario(idUsuario)
);

create table Fan(
	idFan INT (11) NOT NULL,
    apellidos VARCHAR (100),
    sexo VARCHAR(1),
    nacimiento DATE NOT NULL,
    constraint PK_idFan primary key (idFan),
    constraint FK_idFan foreign key (idFan) references Usuario(idUsuario)
);

create table Musico(
	idMusico INT (11) NOT NULL,
    genero INT(11) NOT NULL,
    web VARCHAR (200),
    numComponentes INT(11) NOT NULL,
	constraint PK_idMusico primary key (idMusico),
    constraint FK_idMusico foreign key (idMusico) references Usuario(idUsuario),
    constraint FK_genero foreign key (genero) references Genero(idGenero)
);

create table Concierto(
	idConcierto INT(11) NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(200) NOT NULL,
    estado INT(1) NOT NULL,
    dia DATE NOT NULL,
    hora TIME NOT NULL,
    pago DECIMAL(6,2) NOT NULL,
    locales INT(11) NOT NULL,
    genero INT(11) NOT NULL,
    musico INT(11),
    constraint PK_idConcierto primary key (idConcierto),
    constraint FK_Locales foreign key (locales) references Locales(idLocal),
    constraint FK2_genero foreign key (genero) references Genero(idGenero),
    constraint FK_Musico foreign key (musico) references Musico(idMusico)
);

create table voto_musico(
	fan INT(11) NOT NULL,
    musico INT(11) NOT NULL,
    constraint PK_fanMusico PRIMARY KEY (fan, musico),
    constraint FK_fan foreign key (fan) references Fan(idFan),
    constraint FK2_musico foreign key (musico) references Musico(idMusico)
);

create table voto_concierto(
	fan INT(11) NOT NULL,
    concierto INT(11) NOT NULL,
    constraint PK_fanConcierto PRIMARY KEY (fan, concierto),
    constraint FK2_fan foreign key (fan) references Fan(idFan),
    constraint FK_concierto foreign key (concierto) references Concierto(idConcierto)
);

create table propuesta(
	concierto INT(11) NOT NULL,
    musico INT(11) NOT NULL,
    estado INT(1) NOT NULL,
    constraint PK_conciertoMusico primary key (concierto, musico),
    constraint FK2_concierto foreign key (concierto) references Concierto(idConcierto),
    constraint FK3_musico foreign key (musico) references Musico(idMusico)
);